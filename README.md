
# BedsheetCms -> [Link](https://beddings-development.herokuapp.com/#/)

:key:
Login: `userTester1`
Password: `delanosCompany`

:exclamation: Since app is designed for smaller devices, the controlls are:

- `tap` for edit product (for pc users: `click`)
- `long tap` for delete product (for pc users: `long click`)

Content management system for two products types, having four attributes.

My first steps with Angular, Bootstrap, application deployment.
This project was made to improve my skills at mobile usage of application. 

Version for end-user tests.

My role was Front-end.
Back-end made by teammate.

Project created in four one-week sprints. Started on December 2018.

What I do like:
- Simply and clear layout
- Complementary colors
- UI designed by me (with tester's opinions)
- Footer and it's usage
- Worked in Agile, personally good usage of git in general

What I don't:
- No information about failed operation (why didn't I logged in?)
- Font
- Prompts
- Only Polish version available
- When full screen filled by data, last item partially covered by footer
- Bad understanding of modularity concept in Angular
