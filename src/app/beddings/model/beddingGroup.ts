import {Bedding} from './bedding';

export class BeddingGroup {
  id: string;
  constructor(public width: number,
              public height: number,
              public beddings: Bedding[]) {
    this.id = `b${width}x${height}`;
  }
}
