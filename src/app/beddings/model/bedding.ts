export class Bedding {
  id: number;
  pattern: string;
  width: number;
  height: number;
  onStock: number;
}
