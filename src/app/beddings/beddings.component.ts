import { Component, OnInit } from '@angular/core';
import { Bedding } from './model/bedding';
import { ApiService } from '../shared/api.service';
import {BeddingGroup} from './model/beddingGroup';

@Component({
  selector: 'app-beddings',
  templateUrl: './beddings.component.html',
  styleUrls: ['./beddings.component.css']
})
export class BeddingsComponent implements OnInit {

  model: Bedding = {
    id: undefined,
    pattern: undefined,
    width: undefined,
    height: undefined,
    onStock: undefined
  };
  searchText: string;
  beddingsGroups: BeddingGroup[];

  constructor(private service: ApiService) {
  }

  get getBeddingsGroups() {
    return this.beddingsGroups;
  }

  ngOnInit() {
    this.initBeddings();
  }

  private initBeddings() {
    this.setBeddingsGroupsEmpty();
    this.initWithDataFromDb();
    this.clearModel();
  }

  private setBeddingsGroupsEmpty() {
    this.beddingsGroups = [];
  }

  private initWithDataFromDb() {
    return this.service.getAllBeddings().subscribe(
      res => this.fillBeddings(res),
      err => this.handleErr(err)
    );
  }

  private fillBeddings(res) {
    res.forEach(bedding => {
      const group = this.findGroup(bedding);
      if (!group) {
        this.beddingsGroups.push(
          new BeddingGroup(bedding.width, bedding.height, [ bedding ])
        );
      } else {
        group.beddings.push(bedding);
      }
    });
  }
  private findGroup(bedding: Bedding) {
    let group: BeddingGroup;
    this.beddingsGroups.forEach(bg => {
      if (bg.width === bedding.width && bg.height === bedding.height) {
        group = bg;
      }
    });
    return group;
  }
  private handleErr(err) {
    console.log(err);
  }

  sendNewBedding(): void {
    this.service.postCreateBedding(this.model).subscribe(
      res => this.initBeddings(),
      err => this.handleErr(err)
    );
  }

  askUserForNewOnStock(bedding: Bedding) {
    const newOnStock = this.getNewOnStock(bedding);
    if (this.isOnStockValid(newOnStock)) {
      bedding.onStock = newOnStock;
      this.updateBedding(bedding);
    }
  }

  private getNewOnStock(bedding: Bedding) {
    return parseInt(this.getIntInput(bedding), 10);
  }

  private getIntInput(bedding: Bedding) {
    return prompt(
      `Wzór: ${bedding.pattern}
      Szerokość: ${bedding.width}
      Wysokość: ${bedding.height}
      Aktualny stan: ${bedding.onStock}
      Podaj nowy stan:`,
      `${bedding.onStock}`
    );
  }

  private isOnStockValid(onStock) {
    return !isNaN(onStock) && onStock >= 0;
  }

  updateBedding(bedding: Bedding): void {
    this.service.putBedding(bedding).subscribe(
      res => this.initBeddings(),
      err => this.handleErr(err)
    );
  }

  askForDelete(bedding: Bedding) {
    const deleteAccepted = confirm(
      `Wzór: ${bedding.pattern}
      Szerokość: ${bedding.width}
      Wysokość: ${bedding.height}
      Aktualny stan: ${bedding.onStock}
      Usunąć?`);

    if (deleteAccepted) {
      this.service.deleteBedding(bedding).subscribe(
        res => this.initBeddings(),
        err => this.handleErr(err)
      );
    }
  }

  clearModel() {
    this.model = {
      id: undefined,
      pattern: undefined,
      width: undefined,
      height: undefined,
      onStock: undefined
    };
  }
}
