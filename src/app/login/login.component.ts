import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {User} from './model/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: User = {
    username: undefined,
    password: undefined
  };

  constructor(private service: ApiService,
              private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.service.postLogin(this.model).subscribe(
      res => this.handleRes(res),
      err => this.handleErr(err)
    );
  }

  private handleRes(res) {
    this.saveInLocalStorage(res.token);
    this.router.navigateByUrl('/home');
  }

  private saveInLocalStorage(token) {
    localStorage.setItem('token', token);
  }

  private handleErr(err) {
    console.log(err);
  }

}
