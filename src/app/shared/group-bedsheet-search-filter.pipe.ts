import { Pipe, PipeTransform } from '@angular/core';
import {BedsheetGroup} from '../bedsheets/model/bedsheetGroup';

@Pipe({
  name: 'groupBedsheetSearchFilter'
})
export class GroupBedsheetSearchFilterPipe implements PipeTransform {

  transform(groups: BedsheetGroup[], text: string): BedsheetGroup[] {
    if(text == null || text === '') {
      // console.log(groups);
      return groups;
    }

    let groupsCopy = groups.map( x=> Object.assign({}, x));

    for(let group of groupsCopy) {
      group.bedsheets = group.bedsheets.filter(bedsheet => bedsheet.color.includes(text))
    }

    return groupsCopy.filter(group => group.bedsheets.length !=0)
  }

}
