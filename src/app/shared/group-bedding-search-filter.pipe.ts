import { Pipe, PipeTransform } from '@angular/core';
import {BeddingGroup} from '../beddings/model/beddingGroup';

@Pipe({
  name: 'groupBeddingSearchFilter'
})
export class GroupBeddingSearchFilterPipe implements PipeTransform {

  transform(groups: BeddingGroup[], text: string): BeddingGroup[] {
    if(text == null || text === '') {
      // console.log(groups);
      return groups;
    }

    let groupsCopy = groups.map( x=> Object.assign({}, x));

    for(let group of groupsCopy) {
      group.beddings = group.beddings.filter(bedding => bedding.pattern.includes(text))
    }

    return groupsCopy.filter(group => group.beddings.length !=0)
  }

}
