import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Bedding } from '../beddings/model/bedding';
import { Bedsheet } from '../bedsheets/model/bedsheet';
import { CategoryTile } from '../navigation/model/categoryTile';
import {User} from '../login/model/user';
import {Token} from '../login/model/token';
import {Message} from '../contact/model/Message';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private BEDSHEETS_URL = '/api/bedsheets';
  private BEDDINGS_URL = '/api/beddings';
  private LOGIN_URL = '/login';
  private CONTACT_URL = '/api/feedback'

  constructor(private http: HttpClient) { }

  postLogin(user: User): Observable<Token> {
    return this.http.post<Token>(this.LOGIN_URL, user);
  }

  getAllCategories(): CategoryTile[] {
    return [
      {
        name: 'POŚCIELE',
        path: '/home/beddings'
      },
      {
        name: 'PRZEŚCIERADŁA',
        path: '/home/bedsheets'
      },
    ];
  }

  getAllBeddings(): Observable<Bedding[]> {
    return this.http.get<Bedding[]>(this.BEDDINGS_URL);
  }

  getAllBedsheets(): Observable<Bedsheet[]> {
    return this.http.get<Bedsheet[]>(this.BEDSHEETS_URL);
  }

  postCreateBedding(bedding: Bedding): Observable<string> {
    return this.http.post<string>(this.BEDDINGS_URL, bedding);
  }

  postCreateBedsheet(bedsheet: Bedsheet): Observable<string> {
    return this.http.post<string>(this.BEDSHEETS_URL, bedsheet);
  }

  putBedding(bedding: Bedding): Observable<string> {
    return this.http.put<string>(`${ this.BEDDINGS_URL }/${ bedding.id }`, bedding);
  }

  putBedsheet(bedsheet: Bedsheet): Observable<string> {
    return this.http.put<string>(`${ this.BEDSHEETS_URL }/${ bedsheet.id }`, bedsheet);
  }

  deleteBedding(bedding: Bedding): Observable<string> {
    return this.http.delete<string>(`${ this.BEDDINGS_URL }/${ bedding.id }`);
  }

  deleteBedsheet(bedsheet: Bedsheet): Observable<string> {
    return this.http.delete<string>(`${ this.BEDSHEETS_URL }/${ bedsheet.id }`);
  }

  postSendMessage(message: Message): Observable<string> {
    return this.http.post<string>(this.CONTACT_URL, message);
  }

}
