export class Message {
  author: string;
  title: string;
  message: string;
}
