import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {Message} from './model/Message';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  model: Message = {
    author: undefined,
    title: undefined,
    message: undefined
  };

  constructor(private service: ApiService) { }

  ngOnInit() {
  }

  send() {
    this.service.postSendMessage(this.model).subscribe(
      res => this.handleRes(),
      err => this.handleErr(err)
    )
  }

  private handleRes() {
    alert('Wiadomość pomyślnie wysłana!');
    this.model = {
      author: undefined,
      title: undefined,
      message: undefined
    }
  }

  private handleErr(err) {
    console.log(err);
  }
}
