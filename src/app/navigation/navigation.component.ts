import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { CategoryTile } from './model/categoryTile';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  categoryTiles: CategoryTile[];

  constructor(private service: ApiService) { }

  ngOnInit() {
    this.initializeCategories();
  }

  private initializeCategories = () => {
    this.categoryTiles = this.service.getAllCategories();
  }
}
