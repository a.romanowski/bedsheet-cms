import { Component, OnInit } from '@angular/core';
import { Bedsheet } from './model/bedsheet';
import { ApiService } from '../shared/api.service';
import {BedsheetGroup} from './model/bedsheetGroup';

@Component({
  selector: 'app-bedsheets',
  templateUrl: './bedsheets.component.html',
  styleUrls: ['./bedsheets.component.css']
})
export class BedsheetsComponent implements OnInit {

  model: Bedsheet = {
    id: undefined,
    color: undefined,
    width: undefined,
    height: undefined,
    onStock: undefined
  };
  searchText: string;
  bedsheetsGroups: BedsheetGroup[];

  constructor(private service: ApiService) { }

  get getBedsheetsGroups() {
    return this.bedsheetsGroups;
  }

  ngOnInit() {
    this.initBedsheets();
  }

  initBedsheets() {
    this.setBedsheetsGroupsEmpty();
    this.initWithDataFromDb();
    this.clearModel();
  }

  private setBedsheetsGroupsEmpty() {
    this.bedsheetsGroups = [];
  }

  private initWithDataFromDb() {
    return this.service.getAllBedsheets().subscribe(
      res => this.fillBedsheets(res),
      err => this.handleErr(err)
    );
  }

  fillBedsheets(res) {
    res.forEach(bedsheet => {
      const group = this.findGroup(bedsheet);
      if (!group) {
        this.bedsheetsGroups.push(
          new BedsheetGroup(bedsheet.width, bedsheet.height, [ bedsheet ])
        );
      } else {
        group.bedsheets.push(bedsheet);
      }
    });
  }

  private findGroup(bedsheet: Bedsheet) {
    let group: BedsheetGroup;
    this.bedsheetsGroups.forEach(bs => {
      if (bs.width === bedsheet.width && bs.height === bedsheet.height) {
        group = bs;
      }
    });
    return group;
  }

  private handleErr(err) {
    console.log(err);
  }

  sendNewBedsheet(): void {
    this.service.postCreateBedsheet(this.model).subscribe(
      res => this.initBedsheets(),
      err => this.handleErr(err)
    );
  }


  askUserForNewOnStock(bedsheet: Bedsheet) {
    const newOnStock = this.getNewOnStock(bedsheet);
    if (this.isOnStockValid(newOnStock)) {
      bedsheet.onStock = newOnStock;
      this.updateBedsheet(bedsheet);
    }
  }

  private getNewOnStock(bedsheet: Bedsheet) {
    return parseInt(this.getIntInput(bedsheet), 10);
  }

  private getIntInput(bedsheet: Bedsheet) {
    return prompt(
      `Kolor: ${bedsheet.color}
      Szerokość: ${bedsheet.width}
      Wysokość: ${bedsheet.height}
      Aktualny stan: ${bedsheet.onStock}
      Podaj nowy stan:`,
      `${bedsheet.onStock}`
    );
  }

  private isOnStockValid(onStock) {
    return !isNaN(onStock) && onStock >= 0;
  }

  updateBedsheet(bedsheet: Bedsheet): void {
    this.service.putBedsheet(bedsheet).subscribe(
      res => this.initBedsheets(),
      err => this.handleErr(err)
    );
  }

  askForDelete(bedsheet: Bedsheet) {
    const deleteAccepted = confirm(
      `Kolor: ${bedsheet.color}
      Szerokość: ${bedsheet.width}
      Wysokość: ${bedsheet.height}
      Aktualny stan: ${bedsheet.onStock}
      Usunąć?`);

    if (deleteAccepted) {
      this.service.deleteBedsheet(bedsheet).subscribe(
        res => this.initBedsheets(),
        err => this.handleErr(err)
      );
    }
  }

  clearModel() {
    this.model = {
      id: undefined,
      color: undefined,
      width: undefined,
      height: undefined,
      onStock: undefined
    };
  }
}
