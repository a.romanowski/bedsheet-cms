export class Bedsheet {
  id: number;
  color: string;
  width: number;
  height: number;
  onStock: number;
}
