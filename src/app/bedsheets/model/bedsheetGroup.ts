import {Bedsheet} from './bedsheet';

export class BedsheetGroup {
  id: string;
  constructor(public width: number,
              public height: number,
              public bedsheets: Bedsheet[]) {
    this.id = `b${width}x${height}`;
  }
}
