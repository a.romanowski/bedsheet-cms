import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { BeddingsComponent } from './beddings/beddings.component';
import { BedsheetsComponent } from './bedsheets/bedsheets.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';

import { JwtInterceptor } from './helpers/jwtInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ContactComponent } from './contact/contact.component';
import { GroupBeddingSearchFilterPipe } from './shared/group-bedding-search-filter.pipe';
import { GroupBedsheetSearchFilterPipe } from './shared/group-bedsheet-search-filter.pipe';

const appRoutes: Routes = [
  {
    path: '',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: NavigationComponent,
    children: [
      {
        path: 'beddings',
        component: BeddingsComponent
      },
      {
        path: 'bedsheets',
        component: BedsheetsComponent
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    BeddingsComponent,
    BedsheetsComponent,
    NotFoundComponent,
    LoginComponent,
    ContactComponent,
    GroupBeddingSearchFilterPipe,
    GroupBedsheetSearchFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
